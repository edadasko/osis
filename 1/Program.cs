﻿/*
 * 1.3.2. С клавиатуры вводятся первые символы имени файла (например myfile)
 * и имя файла результата. Необходимо найти все файлы, начинающиеся с данных
 * символов (myfile.txt, myfile1.doc, myfile005.txt, …), по очереди открыть эти файлы
 * и склеить их содержимое в результирующем файле, читая блоками по 256 байт.
*/

using System;
using System.Collections.Generic;
using System.IO;

namespace OSIS
{
    class Program
    {
        static List<string> FindFiles(string[] result, string pattern)
        {
            List<string> foundFiles = new List<string>();
            foreach (string folder in result)
            {
                string[] files = SearchFiles(folder, $"{pattern}*.txt");
                foreach (string file in files)
                {
                    foundFiles.Add(file);
                }
            }

            if (foundFiles.Count != 0)
            {
                Console.WriteLine(string.Join("\n",foundFiles));
            }
            else
            {
                Console.WriteLine("There is not such files");
            }
            return foundFiles;
        }

        static string[] SearchFiles(string path, string pattern)
        {
            string[] searchResult = Directory.GetFiles(path, pattern, SearchOption.AllDirectories);
            return searchResult;
        }

        static string[] SearchDirectory(string path)
        {
            string[] searchResult = Directory.GetDirectories(path);
            return searchResult;
        }

        static void WriteResultToFile(string resultFile, List<string> allFiles)
        {
            List<string> resultText = new List<string>();
            foreach (string file in allFiles)
            {
                resultText.AddRange(File.ReadAllLines(file));
            }
            File.AppendAllLines(resultFile, resultText);
        }

        static void DriveSearch(string resultFile, string pattern)
        {
            string[] result = SearchDirectory(@"\");
            List<string> allFiles = FindFiles(result, pattern);
            WriteResultToFile(resultFile, allFiles);
        }

        static void Main()
        {
            string pattern;
            pattern = Console.ReadLine();

            string resultFile;
            resultFile = Console.ReadLine();

            DriveSearch(resultFile, pattern);
            Console.ReadKey();
        }
    }
}

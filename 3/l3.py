# 3.3.3. Создать двоичное дерево, хранящее числа. Каждый элемент дерева должен хранить число и
# «левую» и «правую» ссылки на нижестоящие элементы. Необходимо реализовать алгоритмы добавления
# числа в двоичное дерево и удаления числа из него. Пользователю должны быть доступны следу­ющие
# функции: добавить число, удалить число, очистить дерево, показать содержимое дерева на экране.
# При выводе на экран показывать дерево в виде строки вида: содержимое родителя (содержимое левой
# ветви, содержимое пра­вой ветви); для каждой из ветвей функция вывода должна быть вызвана
# рекур­сивно.

#!/usr/bin/python

class Tree:
    def __init__(self):
        self.root = None

    def getRoot(self):
        return self.root

    def add(self, val):
        if (self.root == None):
            self.root = Node(val)
        else:
            self._add(val, self.root)

    def _add(self, val, node):
        if (val < node.v):
            if(node.l != None):
                self._add(val, node.l)
            else:
                node.l = Node(val)
        else:
            if (node.r != None):
                self._add(val, node.r)
            else:
                node.r = Node(val)

    def find(self, val):
        if (self.root != None):
            return self._find(val, self.root)
        else:
            return None

    def _find(self, val, node):
        if (val == node.v):
            return node
        elif (val < node.v and node.l != None):
            self._find(val, node.l)
        elif (val > node.v and node.r != None):
            self._find(val, node.r)

    def deleteTree(self):
        self.root = None

    def printTree(self):
        if (self.root != None):
            self._printTree(self.root)

    def _printTree(self, node):
        if (node != None):
            self._printTree(node.l)
            print str(node.v) + ' '
            self._printTree(node.r)

class Node:
    def __init__(self, val):
        self.l = None
        self.r = None
        self.v = val

tree = Tree()

for i in range(-5, 5):
    tree.add(i)

tree.printTree()
print (tree.find(3)).v
print tree.find(10)
tree.deleteTree()
tree.printTree()

// Написать программу, которая создает окно с собственными курсором и иконкой. 
// В главное меню добавить подменю с двумя пунктами Start и Stop. 
// При нажатии на пункт меню Start в окне должна появиться движущаяся по 
// середине окна надпись. После этого выбор пункта меню Stop должно 
// приостанавли­вать движение надписи, а пункта Start – возобновлять. 
// Для реализации движения использовать обработчик сообщения WM_PAINT и таймер.


#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

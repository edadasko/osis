// Написать программу, которая создает окно с двумя элементами управ­ления ListBox, 
// одним Edit и четырьмя Button («Add», «Clear», «ToRight» и «Delete»). 
// При нажатии на кнопку «Add» текст из Edit должен добавляться в первый ListBox,
// если такого текста там еще нет (необходимо выполнить проверку).
// Нажатие кнопки «Clear» очищает оба ListBox-а. Нажатие кнопки «ToRight»
// копирует выделенную строку из первого ListBox во второй 
// (если там еще нет такой строки). Нажатие кнопки «Delete» удаляет выделенные 
// строки в каждом из ListBox-ов.

#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
